﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 movement = new Vector3 ( Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical") );
		GetComponent<Rigidbody> ().AddForce (movement * 200);
		GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
	}
}
