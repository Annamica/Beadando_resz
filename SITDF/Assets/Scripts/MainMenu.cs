﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class MainMenu : MonoBehaviour {

	public Text MenuText;
	public Text Text1, Text2, Text3;


	void Start () {
		MenuText.text = "Menu";
		//try{
			StreamReader Olvaso = new StreamReader ("Save1.txt");
			string Sor = Olvaso.ReadToEnd ();
			Text1.text = Sor;
			Olvaso = new StreamReader ("Save2.txt");
			Sor = Olvaso.ReadToEnd ();
			Text2.text = Sor;
			Olvaso = new StreamReader ("Save3.txt");
			Sor = Olvaso.ReadToEnd ();
			Text3.text = Sor;
		//}catch(IOException e){
		//	Debug.Log ("Hiba a Save1-2-3.text beolvasásnál");
		//}
	}

	void Update () {

	}

		//New
	public void Hiba(string tmp, string tmp2){
		try{
			StreamWriter Iro = new StreamWriter (tmp);
			Iro.Write (tmp2 + " H100 E100 C0 S1 I1-1 I2-1 T1-1 T5-1 T9-1 T13-1");
			Iro.Flush ();
			Iro.Close ();
			SceneManager.LoadScene ("Game");
		}catch(IOException e){
			SceneManager.LoadScene ("Game");
		}
	}

	public void New1(){
		Hiba ("Save1.txt", "Jatekos1");
	}

	public void New2(){
		Hiba ("Save2.txt", "Jatekos2");
	}

	public void New3(){
		Hiba ("Save3.txt", "Jatekos3");
	}
		//Load
	public void Betolt(string tmp){
		StreamReader Olvaso = new StreamReader (tmp);
		string Sor = Olvaso.ReadToEnd ();
		SceneManager.LoadScene ("Game");
	}

	public void Load1(){
		Betolt ("Save1.txt");
	}
		
	public void Load2(){
		Betolt ("Save2.txt");
	}

	public void Load3(){
		Betolt ("Save3.txt");
	}

		//Credits
	public void Credits(){
		MenuText.text = "Persics Anna \nDemeter Mátyás";

	}

		//Exit
	public void Exit(){
		MenuText.text = "By!";
		Application.Quit ();
	}
}
