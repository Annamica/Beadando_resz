﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {

	public Text Cim;
	public GameObject Panel;
	public GameObject Panel2;
	public GameObject PanelChat;

	private int mozog = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var player = GameObject.FindGameObjectWithTag ("Player");
		var player2 = GameObject.FindGameObjectWithTag ("Player2");
		var respawn = GameObject.FindGameObjectWithTag ("Respawn");
		var respawn2 = GameObject.FindGameObjectWithTag ("Respawn2");
		if (mozog == 1) {
			player.transform.position = respawn.transform.position;
			player2.transform.position = respawn2.transform.position;
			mozog = 0;
		}
	}

	public void Inventory(){
		Time.timeScale = 0.0f;
		Panel2.SetActive (true);
		Panel.SetActive (false);
		PanelChat.SetActive (false);
		Cim.text = "Inventory";
		mozog = 1;
	}

	public void Character(){
		Time.timeScale = 0.0f;
		Panel2.SetActive (true);
		Panel.SetActive (false);
		PanelChat.SetActive (false);
		Cim.text = "Character";
		mozog = 1;
	}

	public void Close(){
		Time.timeScale = 1.0f;
		Panel2.SetActive (false);
		Panel.SetActive (true);
		PanelChat.SetActive (false);
		Cim.text = " ";
	}
}
