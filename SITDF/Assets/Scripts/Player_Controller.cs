﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Player_Controller : MonoBehaviour {

	public Text Erint;
	public Text Health;
	public Text Energy;
	public GameObject Panel;
	public GameObject Panel2;
	public GameObject PanelChat;

	//tsuba Place
	public Image TsubaPlace1;
	public Image TsubaPlace2;
	public Image TsubaPlace3;
	public Image TsubaPlace4;
	public Image TsubaPlace5;
	public Image TsubaPlace6;
	public Image TsubaPlace7;
	public Image TsubaPlace8;
	public Image TsubaPlace9;

	//Tsuba Sprite
	//
	public Sprite TsubaNo;
	public Sprite TsubaPicture1;
	public Sprite TsubaPicture2;
	//public Sprite TsubaPicture3;
	public Sprite TsubaPicture4;
	public Sprite TsubaPicture5;
	//public Sprite TsubaPicture6;
	public Sprite TsubaPicture7;
	public Sprite TsubaPicture8;
	//public Sprite TsubaPicture9;
	public Sprite TsubaPicture10;
	public Sprite TsubaPicture11;
	//public Sprite TsubaPicture11;

	public int health = 100;
	public int energy = 100;
	public int coin = 100;
	public int demage = 5;
	public int deffend = 7;
	public float speed = 2.0f;
	//public Button Map;

	//tsuba attack
	public GameObject tsubaAtack1;
	public GameObject tsubaAtack2;
	//public GameObject tsubaAttack4;


	void Start () {
		Erint.text = "Move: WASD ";
		Panel2.SetActive (false);
		PanelChat.SetActive (false);
		//tsuba attack 1;
		tsubaAtack1.GetComponent<ParticleSystem> ().Stop ();
		tsubaAtack2.GetComponent<ParticleSystem> ().Stop ();
		//tsubaAttack4.GetComponent<ParticleSystem> ().Stop ();
	}

	void Update () {
		// ---tag alapu hívás
		var enemy = GameObject.FindGameObjectWithTag ("Enemy");
		var player2 = GameObject.FindGameObjectWithTag ("Player2");
		var smooth = GameObject.FindGameObjectWithTag ("Smooth");
		var shop = GameObject.FindGameObjectWithTag ("Shop");
		var respawn2 = GameObject.FindGameObjectWithTag ("Respawn2");
		var tsuba1 = GameObject.FindGameObjectWithTag ("Tsuba1");
		var tsuba2 = GameObject.FindGameObjectWithTag ("Tsuba2");

		// ---adatok változtatása
		Health.text = health.ToString();
		Energy.text = energy.ToString ();

		// ---mozgás
		Vector3 movement = new Vector3 ( Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical") );
		GetComponent<Rigidbody> ().AddForce ((movement * 100)*speed);
		GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);


		//  ---Tsubahasználat
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Tsuba1Aktiv ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Tsuba2Aktiv ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Tsuba3Aktiv ();
		}


		// ---battle teleport
		if (Vector3.Distance (transform.position, enemy.transform.position) <= 4.0f) {
			if (Input.GetKeyDown (KeyCode.E)) {
				transform.position = new Vector3 (0.0f, 0.7f, 0.0f);
				smooth.transform.position = new Vector3 (0, 2, 0);
				player2.transform.position = respawn2.transform.position;
			}
		}
		//  ---shop
		if (Vector3.Distance (transform.position, shop.transform.position) <= 2.0f) {
			if (Input.GetKeyDown (KeyCode.E)) {
				Panel.SetActive(false);
				Panel2.SetActive (false);
				PanelChat.SetActive (true);
			}
		}
		//  --tsuba1
		if(tsuba1 != null){
			if (Vector3.Distance (transform.position, tsuba1.transform.position) <= 2.0f) {
				if (Input.GetKeyDown (KeyCode.E)) {
					if (TsubaPlace1.sprite == TsubaNo) {
						TsubaPlace1.sprite = TsubaPicture1;
					}
					else if (TsubaPlace2.sprite == TsubaNo) {
						TsubaPlace2.sprite = TsubaPicture1;
					}
					else if (TsubaPlace3.sprite == TsubaNo) {
						TsubaPlace3.sprite = TsubaPicture1;
					} else {
						Erint.text = "You earned a tsuba!";
					}
					GameObject.Destroy (tsuba1);
					Erint.text = "You earned a tsuba!";
				}
			}				
		} else {
			GameObject.Destroy (tsuba1);
		}
		//  --tsuba2
		if(tsuba2 != null){			//here
			if (Vector3.Distance (transform.position, tsuba2.transform.position) <= 2.0f) {			//here
				if (Input.GetKeyDown (KeyCode.E)) {
					if (TsubaPlace1.sprite == TsubaNo) {
						TsubaPlace1.sprite = TsubaPicture2;			//here
					}
					else if (TsubaPlace2.sprite == TsubaNo) {
						TsubaPlace2.sprite = TsubaPicture2;			//here
					}
					else if (TsubaPlace3.sprite == TsubaNo) {
						TsubaPlace3.sprite = TsubaPicture2;			//here
					} else {
						Erint.text = "You earned a tsuba!";
					}
					GameObject.Destroy (tsuba2);					//here
					Erint.text = "You earned a tsuba!";
				}
			}				
		} else {
			GameObject.Destroy (tsuba2);							// here
		}
	}


	void Tsuba1Aktiv(){
		
		if (TsubaPlace1.sprite == TsubaPicture1) {
			tsubaAtack1.transform.position = transform.position;
			tsubaAtack1.GetComponent<ParticleSystem> ().Play ();
		}
		if (TsubaPlace1.sprite == TsubaPicture2) {
			tsubaAtack2.transform.position = transform.position;
			tsubaAtack2.GetComponent<ParticleSystem> ().Play ();
		}
	}

	void Tsuba2Aktiv(){
		if (TsubaPlace2.sprite == TsubaPicture1) {
			tsubaAtack1.transform.position = transform.position;
			tsubaAtack1.GetComponent<ParticleSystem> ().Play ();
		}
		if (TsubaPlace2.sprite == TsubaPicture2) {
			tsubaAtack2.transform.position = transform.position;
			tsubaAtack2.GetComponent<ParticleSystem> ().Play ();
		}
	}

	void Tsuba3Aktiv(){
		if (TsubaPlace3.sprite == TsubaPicture1) {
			tsubaAtack1.transform.position = transform.position;
			tsubaAtack1.GetComponent<ParticleSystem> ().Play ();
		}
		if (TsubaPlace3.sprite == TsubaPicture2) {
			tsubaAtack2.transform.position = transform.position;
			tsubaAtack2.GetComponent<ParticleSystem> ().Play ();
		}
	}

	// ---trigger kocka érzékelő
	void OnTriggerEnter(Collider other)
	{
		Erint.text = "Press E";
		/*if (other.tag == "Tsuba1") {
			
		}*/
	}
	// ---trigger kocka kilépő
	void OnTriggerExit(Collider collider){
		Erint.text = " ";
	}
}
